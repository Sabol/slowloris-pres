#!/bin/python3

import sys
import subprocess
import os 

'''
Slow Loris DDoS demo for CSC 495 - Network Security

Author: Cory Sabol

This script simply launches three Docker containers.
    two of which will be running an instance of the slowloris program and the
    third will be running a vulnerable Flask webapp
'''
PATH1 = "./containers/webapp/Dockerfile"
PATH2 = ""
APPNAME = "app_cont"
SLNAME = "sl_cont"

# Python dict to hold commands to be run
cmds = { "bld_app_cont" : "docker build -t " + APPNAME + " -f " + PATH1 + " .",
         "bld_sl_cont"  : "docker build -t " + SLNAME + " " + PATH2,
         "run_app_cont" : "docker run -v \"../../server/app/:/var/www/\"./containers/loris/Dockerfile " + APPNAME,
         "run_sl_cont"  : "docker run -v \"../../loris/slowloris.pl:\" " + SLNAME,
         "get_ip"       : "docker inspect --format \"{{.NetworkSettings.IPAddress}}\" $(docker ps -q)"
       }

# the image id's of the containers
# order matters.
iids = []

# the id's of the containers, used to get the ip addresses
cids = []

ips = { "app_ip" : "",
        "sl_ip0" : "",
        "sl_ip1" : ""
      }

def printstd(stdout, stderr, buffsize):
# Kill the script on error
    if stderr:
        for line in stderr.readline():
            print(line)
        sys.exit()
    
    count = 0
    for line in stdout.readline():
        print(line)
        count = count + 1
        if count >= buffsize:
            return

def execcmd(cmd, block):
    process = None
    if (block == False):
        process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
        stdout = process.stdout
        stderr = process.stderr

        # Print the output, this of course will be after the fact of the process
        # executing, but we don't want it to block
        printstd(stdout, stderr, 30)
    else:
        process = subprocess.Popen(cmd.split())

def ping():
    None

# Do we need to be root?
'''
The big idea for this script;
First first the script should check to see if the containers are already built.
First we spin up the containers, starting with the webapp container.
After each container is built we get the ipaddress of it and store it in a dict.
The script will then ping the webapp for a response, if it's good to go then
'''

def main():
    
    APP_UP = False
    APP_IP = ""
    TEMP_NAME = ""
    # get the commandline args
    '''
    PATH1 = sys.argv[1] # path to the first Dockerfile
    PATH2 = sys.argv[2] # path to the second Dockerfile
    '''

    # execute the commands in order
    # build app container
    execcmd(cmds["bld_app_cont"], True)
    # get the ip of the app_container
    #execcmd(cmds["get_ip"])

    # run app container
    '''
    execcmd(cmds["run_app_cont"])
    # build loris container 1
    TEMPNAME = SLNAME + "1"
    execcmd(cmds["bld_sl_cont"])
    # build loris container 2
    TEMPNAME = SLNAME + "2"
    execcmd(cmds["bld_sl_cont"])
    '''
    # ping the web app 
    # ping(ips["app_ip"])

if __name__ == "__main__":
    main()
